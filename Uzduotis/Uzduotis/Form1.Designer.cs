﻿namespace Uzduotis
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Menuo = new System.Windows.Forms.TextBox();
            this.Raud = new System.Windows.Forms.TextBox();
            this.Bara = new System.Windows.Forms.TextBox();
            this.Mase = new System.Windows.Forms.TextBox();
            this.Diena = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.skBara = new System.Windows.Forms.TextBox();
            this.skRaud = new System.Windows.Forms.TextBox();
            this.rezultatas = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Menuo
            // 
            this.Menuo.Location = new System.Drawing.Point(58, 6);
            this.Menuo.Name = "Menuo";
            this.Menuo.Size = new System.Drawing.Size(91, 20);
            this.Menuo.TabIndex = 0;
            // 
            // Raud
            // 
            this.Raud.Location = new System.Drawing.Point(58, 84);
            this.Raud.Name = "Raud";
            this.Raud.Size = new System.Drawing.Size(91, 20);
            this.Raud.TabIndex = 1;
            // 
            // Bara
            // 
            this.Bara.Location = new System.Drawing.Point(58, 110);
            this.Bara.Name = "Bara";
            this.Bara.Size = new System.Drawing.Size(91, 20);
            this.Bara.TabIndex = 2;
            // 
            // Mase
            // 
            this.Mase.Location = new System.Drawing.Point(58, 58);
            this.Mase.Name = "Mase";
            this.Mase.Size = new System.Drawing.Size(91, 20);
            this.Mase.TabIndex = 3;
            // 
            // Diena
            // 
            this.Diena.Location = new System.Drawing.Point(58, 32);
            this.Diena.Name = "Diena";
            this.Diena.Size = new System.Drawing.Size(91, 20);
            this.Diena.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Menuo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Diena";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Mase";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Raud";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Bara";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(15, 136);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(134, 29);
            this.button1.TabIndex = 10;
            this.button1.Text = "Issaugoti";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // skBara
            // 
            this.skBara.Location = new System.Drawing.Point(87, 326);
            this.skBara.Name = "skBara";
            this.skBara.Size = new System.Drawing.Size(62, 20);
            this.skBara.TabIndex = 12;
            // 
            // skRaud
            // 
            this.skRaud.Location = new System.Drawing.Point(87, 207);
            this.skRaud.Multiline = true;
            this.skRaud.Name = "skRaud";
            this.skRaud.Size = new System.Drawing.Size(112, 95);
            this.skRaud.TabIndex = 11;
            // 
            // rezultatas
            // 
            this.rezultatas.Location = new System.Drawing.Point(3, 220);
            this.rezultatas.Name = "rezultatas";
            this.rezultatas.Size = new System.Drawing.Size(78, 29);
            this.rezultatas.TabIndex = 13;
            this.rezultatas.Text = "rezultas";
            this.rezultatas.UseVisualStyleBackColor = true;
            this.rezultatas.Click += new System.EventHandler(this.rezultatas_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(225, 461);
            this.Controls.Add(this.rezultatas);
            this.Controls.Add(this.skBara);
            this.Controls.Add(this.skRaud);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Diena);
            this.Controls.Add(this.Mase);
            this.Controls.Add(this.Bara);
            this.Controls.Add(this.Raud);
            this.Controls.Add(this.Menuo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Menuo;
        private System.Windows.Forms.TextBox Raud;
        private System.Windows.Forms.TextBox Bara;
        private System.Windows.Forms.TextBox Mase;
        private System.Windows.Forms.TextBox Diena;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox skBara;
        private System.Windows.Forms.TextBox skRaud;
        private System.Windows.Forms.Button rezultatas;
    }
}

