﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Uzduotis
{
    public partial class Form1 : Form
    {
        const string failai = "grybai.txt";
        public Form1()
        {
            InitializeComponent();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                double menuo = Convert.ToDouble(Menuo.Text);
                double diena = Convert.ToDouble(Diena.Text);
                double mase = Convert.ToDouble(Mase.Text);
                double raud = Convert.ToDouble(Raud.Text);
                double bara = Convert.ToDouble(Bara.Text);

                File.AppendAllText(failai, menuo + " " + diena + " " + mase + " " +
                    raud + " " + bara);
                File.AppendAllText(failai, Environment.NewLine);

            }
            catch
            {
                MessageBox.Show("Klaida");

            }

        }

        private void rezultatas_Click(object sender, EventArgs e)

        { 

            string[] sk = File.ReadAllLines(failai);

            double [] menuo1 =new double[sk.Length];
            double [] diena1 = new double[sk.Length];
            double [] mase1 = new double[sk.Length];
            double [] raud1 = new double[sk.Length];
            double [] bara1 = new double[sk.Length];

            for (int i = 0; i < sk.Length; i++)
            {
                string [] items = sk[i].Split(' ');

                menuo1 [i] = Convert.ToDouble(items[0]);
                diena1 [i] = Convert.ToDouble(items[1]);
                mase1 [i] = Convert.ToDouble(items[2]);
                raud1 [i] = Convert.ToDouble(items[3]);
                bara1 [i] = Convert.ToDouble(items[4]);


            }
            double did = mase1[0];
            int didindex = 0;
            for (int i = 0; i < mase1.Length; i++)
            {
                if(mase1[i]>did)
                {
                    did = mase1[i];
                    didindex = i;
                }
            }
            skRaud.Text = menuo1[didindex].ToString();
            skBara.Text = diena1[didindex].ToString();
        }
    }

}